# Test Driven Development of Modified Chess

Implements a toy chess program that sets up pieces on a chessboard,
moves thems, and finds possible moves for a given piece in a
certain position.

Simplified Rules:
  - Queen and knight cannot move
  - King can only move one square, it cannot castle
  - Pawns follow standard moves but cannot en passant capture or promote at the end of the board
  - Rooks and bishops follow standard rules  
  
JUnit test classes are created for the chess board and each of the pieces.  
All test classes are included in ChessSuite, which is the main method.


