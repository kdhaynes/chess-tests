package a1;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import a1.ChessPiece.Color;
import java.util.ArrayList;
import java.util.Collections;

public class PawnTest {

	private ChessBoard rboard;
	private ChessPiece myPiece;
	private Pawn wp;
	private Pawn bp;
	private String value;
	private ArrayList<String> myList;

	@Before 
	public void setUp() {
		rboard = new ChessBoard();
		rboard.initialize();
		wp = new Pawn(rboard,Color.WHITE);
		bp = new Pawn(rboard,Color.BLACK);
		value = "";
		myList = new ArrayList<String>();
	}

	////TEST STRINGS
	@Test
	public void testBlackPawnString() {
		value = bp.toString();
		assertEquals(value,"\u265F");
	}

	@Test
	public void testWhitePawnString() {
		value = wp.toString();
		assertEquals(value,"\u2659");
	}

	///TEST BLACK LEGALMOVES
	@Test
	public void testBlackLegalMovesEnd() {
		rboard.placePiece(bp,  "d1");
		ArrayList<String> mymoves = bp.legalMoves();
		ArrayList<String> myList = new ArrayList<String>();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackLegalMovesInit() {
		// Test
		try {
			myPiece = rboard.getPiece("a7");
		} catch (IllegalPositionException e) {
			fail("Illegal Get Piece Testing Legal Moves");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();

		// Evaluate
		ArrayList<String> mylist = new ArrayList<String>();
		mylist.add("a6");
		mylist.add("a5");
		Collections.sort(mymoves);
		Collections.sort(mylist);
		assertEquals(mymoves,mylist);
	}

	@Test
	public void testBlackLegalMovesBBlock() {
		// Test
		Boolean ptest = rboard.placePiece(bp,"g6");
		if (ptest == false) {
			fail("Bad Place Black Pawn G6");
		}
		try {
			myPiece = rboard.getPiece("g7");
		} catch (IllegalPositionException e) {
			fail("Bad Get G7");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackLegalMovesWBlock() {
		Boolean ptest = rboard.placePiece(wp,"g6");
		if (ptest == false) {
			fail("Bad Place White Pawn G6");
		}
		try {
			myPiece = rboard.getPiece("g7");
		} catch (IllegalPositionException e) {
			fail("Bad Get G7");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackLegalMovesNoCapture() {
		Boolean ptest = rboard.placePiece(bp,"e6");
		if (ptest == false) {
			fail("Black Legal Moves 1");
		}
		ptest = rboard.placePiece(wp, "d6");
		if (ptest == false) {
			fail("Black Legal Moves 2");
		}
		try {
			myPiece = rboard.getPiece("d7");
		} catch (IllegalPositionException e) {
			fail("Bad Get D7");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackLegalMovesCapture() {
		//Set Up
		Boolean ptest = rboard.placePiece(wp,"e6");
		if (ptest == false) {
			fail("Black Legal Moves 1");
		}
		ptest = rboard.placePiece(bp, "d6");
		if (ptest == false) {
			fail("Black Legal Moves 2");
		}
		Pawn wp2 = new Pawn(rboard,Color.WHITE);
		ptest = rboard.placePiece(wp2,"c6");
		if (ptest == false) {
			fail("Black Legal Moves 3");
		}

		try {
			myPiece = rboard.getPiece("d7");
		} catch (IllegalPositionException e) {
			fail("Bad Get D7");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		Collections.sort(mymoves);
		myList.add("e6");
		myList.add("c6");
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	///TEST WHITE LEGALMOVES
	@Test
	public void testWhiteLegalMovesEnd() {
		rboard.placePiece(wp,  "d8");
		ArrayList<String> mymoves = wp.legalMoves();
		ArrayList<String> myList = new ArrayList<String>();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testWhiteLegalMovesInit() {
		try {
			myPiece = rboard.getPiece("a2");
		} catch (IllegalPositionException e) {
			fail("Illegal Get Piece Testing Legal Moves");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		ArrayList<String> mylist = new ArrayList<String>();
		mylist.add("a3");
		mylist.add("a4");
		Collections.sort(mymoves);
		Collections.sort(mylist);
		assertEquals(mymoves,mylist);
	}

	@Test
	public void testWhiteLegalMovesBBlock() {
		Boolean ptest = rboard.placePiece(bp,"g3");
		if (ptest == false) {
			fail("Bad Place Black Pawn G3");
		}
		try {
			myPiece = rboard.getPiece("g2");
		} catch (IllegalPositionException e) {
			fail("Bad Get G2");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testWhiteLegalMovesWBlock() {
		Boolean ptest = rboard.placePiece(wp,"g3");
		if (ptest == false) {
			fail("Bad Place White Pawn G3");
		}
		try {
			myPiece = rboard.getPiece("g2");
		} catch (IllegalPositionException e) {
			fail("Bad Get G2");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testWhiteLegalMovesNoCapture() {
		Boolean ptest = rboard.placePiece(bp,"d3");
		if (ptest == false) {
			fail("White Legal Moves 1");
		}
		ptest = rboard.placePiece(wp, "e3");
		if (ptest == false) {
			fail("White Legal Moves 2");
		}
		try {
			myPiece = rboard.getPiece("d2");
		} catch (IllegalPositionException e) {
			fail("Bad Get D2");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testWhiteLegalMovesCapture() {
		Boolean ptest = rboard.placePiece(wp,"e3");
		if (ptest == false) {
			fail("White Legal Moves 1");
		}
		ptest = rboard.placePiece(bp, "d3");
		if (ptest == false) {
			fail("White Legal Moves 2");
		}
		Pawn bp2 = new Pawn(rboard,Color.BLACK);
		ptest = rboard.placePiece(bp2, "f3");
		if (ptest == false) {
			fail("White Legal Moves 3");
		}

		try {
			myPiece = rboard.getPiece("e2");
		} catch (IllegalPositionException e) {
			fail("Bad Get D2");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		Collections.sort(mymoves);
		myList.add("d3");
		myList.add("f3");
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

}
