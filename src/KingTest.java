package a1;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import a1.ChessPiece.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class KingTest {

	private ChessBoard rboard;
	private ChessPiece myPiece;
	private King wk;
	private King bk;
	private String value;
	private ArrayList<String> myMoves;
	private ArrayList<String> myList;

	@Before
	public void setUp() {
		rboard = new ChessBoard();
		rboard.initialize();
		wk = new King(rboard,Color.WHITE);
		bk = new King(rboard,Color.BLACK);
		value = "";
		myList = new ArrayList<String>();
	}


	////TEST STRINGS
	@Test
	public void testBlackKingString() {
		value = bk.toString();
		assertEquals(value,"\u265A");
	}

	@Test
	public void testWhiteKingString() {
		value = wk.toString();
		assertEquals(value,"\u2654");
	}

	///TEST LEGAL MOVES
	@Test
	public void testBlackKingBlockedLegalMoves() {
		try {
			myPiece = rboard.getPiece("e8");
		} catch (IllegalPositionException e) {
			fail ("Illegal King E8 Get");
		}
		myMoves = myPiece.legalMoves();
		assertEquals(myMoves,myList);
	}

	@Test
	public void testWhiteKingBlockedLegalMoves() {
		try {
			myPiece = rboard.getPiece("e1");
		} catch (IllegalPositionException e) {
			fail ("Illegal King E1 Get");
		}
		myMoves = myPiece.legalMoves();
		assertEquals(myMoves,myList);
	}

	@Test
	public void testKingCenterLegalMoves() {
		Boolean ptest = rboard.placePiece(wk, "e4");
		if (ptest == false) {
			fail("Illegal Place King E4");
		}
		ArrayList<String> mymoves = wk.legalMoves();
		List<String> myListAdd1 = Arrays.asList("e3","e5","d4","f4");
		myList.addAll(myListAdd1);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testWhiteKingBlockTakeLegalMoves() {
		Boolean ptest = rboard.placePiece(wk, "d3");
		if (ptest == false) {
			fail("Illegal Place King D3");
		}
		ptest = rboard.placePiece(bk,  "e3");
		if (ptest == false) {
			fail("Illegal Place King E3");
		}
		ArrayList<String> mymoves = wk.legalMoves();
		List<String> myListAdd1 = Arrays.asList("c3","d4","e3");
		myList.addAll(myListAdd1);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackKingBlockTakeLegalMoves() {
		Boolean ptest = rboard.placePiece(bk, "a7");
		if (ptest == false) {
			fail("Illegal Place King A7");
		}
		ptest = rboard.placePiece(wk,  "a6");
		if (ptest == false) {
			fail("Illegal Place King A6");
		}
		ArrayList<String> mymoves = bk.legalMoves();
		myList.add("a6");
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBlackKingRightEdge() {
		Boolean ptest = rboard.placePiece(bk, "h3");
		if (ptest == false) {
			fail("Illegal Place King H3");
		}
		ArrayList<String> mymoves = bk.legalMoves();
		List<String> myListAdd1 = Arrays.asList("h4","h2","g3");
		myList.addAll(myListAdd1);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

}
