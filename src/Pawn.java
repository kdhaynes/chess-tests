package a1;

import java.util.ArrayList;

public class Pawn extends ChessPiece {

	public Pawn(ChessBoard board, Color color) {
		super(board, color);
	}

	@Override
	public String toString() {
		if (color == Color.BLACK) {
			return "\u265F";
		} else {
			return "\u2659";
		}
	}

	@Override
	public ArrayList<String> legalMoves() {
		ArrayList<String> mymoves = new ArrayList<String>();

		if (color == Color.BLACK) {
			// Check moves forward
			legalAddForwards(0,-1,mymoves);
			// Check move left forward
			legalAddTake(1,-1,mymoves);
			// Check move right forward
			legalAddTake(-1,-1,mymoves);
		}

		if (color == Color.WHITE) {
			// Check moves forward
			legalAddForwards(0,1,mymoves);
			// Check move  left forward
			legalAddTake(-1,1,mymoves);
			// Check move right forward
			legalAddTake(1,1,mymoves);
		}
		return mymoves;
	}

	private String getNewPosition(int newcol, int newrow) {
		String newpos = colname[newcol].concat(rowname[newrow]);
		return newpos;
	}

	private ArrayList<String> legalAddForwards(int cdir, int rdir, 
			ArrayList<String> mymoves) {

		ChessPiece trypiece;
		String trypos;
		int trycol = column;
		int tryrow = row;

		trycol += cdir;
		tryrow += rdir;
		if ((trycol > -1) && (trycol < 8) && (tryrow > -1) && (tryrow < 8)) {
			trypos = getNewPosition(trycol,tryrow);
			try {
				trypiece = board.getPiece(trypos);
				if (trypiece == null) {
					mymoves.add(trypos);

					trycol += cdir;
					tryrow += rdir;
					trypos = getNewPosition(trycol,tryrow);
					try {
						trypiece = board.getPiece(trypos);
						if (trypiece == null) {
							mymoves.add(trypos);
						}
					} catch (IllegalPositionException e) {
					}
				}
			} catch (IllegalPositionException e) {
			}
		} 
		return mymoves;
	}

	private ArrayList<String> legalAddTake(int cdir, int rdir, 
			ArrayList<String> mymoves) {

		ChessPiece trypiece;
		String trypos;
		int trycol = column;
		int tryrow = row;

		trycol += cdir;
		tryrow += rdir;
		if ((trycol > -1) && (trycol < 8) && (tryrow > -1) && (tryrow < 8)) {
			trypos = getNewPosition(trycol,tryrow);
			try {
				trypiece = board.getPiece(trypos);
				if (trypiece == null) {
				} else if (trypiece.color != color) {
					mymoves.add(trypos);
				}
			} catch (IllegalPositionException e) {
			}
		} 

		return mymoves;
	}

}
