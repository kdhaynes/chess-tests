package a1;


import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import junit.framework.JUnit4TestAdapter;

//This section declares all of the test classes in the program.
@RunWith (Suite.class)
@Suite.SuiteClasses ({ ChessBoardTest.class, PawnTest.class, 
	KnightTest.class, QueenTest.class, KingTest.class,
	BishopTest.class, RookTest.class})

public class ChessSuite {

	public static void main (String[] args)
	{
		Result result = JUnitCore.runClasses(Suite.class);
	}

}
