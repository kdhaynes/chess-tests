package a1;

import java.util.ArrayList;

public class King extends ChessPiece {

	public King(ChessBoard board, Color color) {
		super(board, color);
	}

	@Override
	public String toString() {
		if (color == Color.BLACK) {
			return "\u265A";
		} else {
			return "\u2654";
		}
	}

	@Override
	public ArrayList<String> legalMoves() {
		ArrayList<String> mymoves = new ArrayList<String>();

		// Check up
		mymoves = legalAdds(0,1,mymoves);
		// Check down
		mymoves = legalAdds(0,-1,mymoves);
		// Check left
		mymoves = legalAdds(-1,0,mymoves);
		// Check right
		mymoves = legalAdds(1,0,mymoves);

		return mymoves;
	}

	private String getNewPosition(int newcol, int newrow) {
		String newpos = colname[newcol].concat(rowname[newrow]);
		return newpos;
	}

	private ArrayList<String> legalAdds(int cdir, int rdir, 
			ArrayList<String> mymoves) {
		
		ChessPiece trypiece;
		String trypos;
		int trycol = column;
		int tryrow = row;

		trycol += cdir;
		tryrow += rdir;
		if ((trycol > -1) && (trycol < 8) && (tryrow > -1) && (tryrow < 8)) {
			trypos = getNewPosition(trycol,tryrow);
			try {
				trypiece = board.getPiece(trypos);
				if (trypiece == null) {
					mymoves.add(trypos);
				} else if (trypiece.getColor() != color) {
					mymoves.add(trypos);
				}
			} catch (IllegalPositionException e) {
			}
		} 
		return mymoves;
	}

}
