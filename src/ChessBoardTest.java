package a1;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import a1.ChessPiece.Color;

public class ChessBoardTest {

	private ChessBoard cboard;
	private ChessPiece cpiece;
	private ChessPiece bpiece;
	private ChessPiece wpiece;
	protected String[] rowname = {"1","2","3","4","5","6","7","8"};
	protected String[] colname = {"a","b","c","d","e","f","g","h"};

	@Before
	public void setUp() {
		cboard = new ChessBoard();
		cboard.initialize();

		cpiece = null;
		bpiece = new Pawn(cboard,Color.BLACK);
		wpiece = new Pawn(cboard,Color.WHITE);
	}

	//GET PIECE TESTS
	@Test(expected = IllegalPositionException.class)
	public void testGetPiece0() throws IllegalPositionException {
		cboard.getPiece("a0");
	}

	@Test(expected = IllegalPositionException.class)
	public void testGetPiece9() throws IllegalPositionException {
		cboard.getPiece("g9");
	}

	@Test(expected = IllegalPositionException.class)
	public void testGetPieceI() throws IllegalPositionException {
		cboard.getPiece("i7");
	}

	@Test
	public void testInitA1() {
		try {
			cpiece = cboard.getPiece("a1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial a1");
		}
		assertEquals(true,(cpiece instanceof Rook));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitA2() {
		try {
			cpiece = cboard.getPiece("a2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial a2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitA7() {
		try {
			cpiece = cboard.getPiece("a7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial a7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitA8(){ 
		try {
			cpiece = cboard.getPiece("a8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial a8");
		}
		assertEquals(true,(cpiece instanceof Rook));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitB1() {
		try {
			cpiece = cboard.getPiece("b1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial b1");
		}
		assertEquals(true,(cpiece instanceof Knight));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitB2() {
		try {
			cpiece = cboard.getPiece("b2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial b2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitB7() {
		try {
			cpiece = cboard.getPiece("b7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial b7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitB8(){ 
		try {
			cpiece = cboard.getPiece("b8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial b8");
		}
		assertEquals(true,(cpiece instanceof Knight));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitC1() {
		try {
			cpiece = cboard.getPiece("c1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial c1");
		}
		assertEquals(true,(cpiece instanceof Bishop));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitC2() {
		try {
			cpiece = cboard.getPiece("c2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial c2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitC7() {
		try {
			cpiece = cboard.getPiece("c7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial c7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitC8(){ 
		try {
			cpiece = cboard.getPiece("c8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial a8");
		}
		assertEquals(true,(cpiece instanceof Bishop));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitD1() {
		try {
			cpiece = cboard.getPiece("d1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial d1");
		}
		assertEquals(true,(cpiece instanceof Queen));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitD2() {
		try {
			cpiece = cboard.getPiece("d2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial d2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitD7() {
		try {
			cpiece = cboard.getPiece("d7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial d7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitD8(){ 
		try {
			cpiece = cboard.getPiece("d8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial d8");
		}
		assertEquals(true,(cpiece instanceof Queen));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitE1() {
		try {
			cpiece = cboard.getPiece("e1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial e1");
		}
		assertEquals(true,(cpiece instanceof King));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitE2() {
		try {
			cpiece = cboard.getPiece("e2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial e2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitE7() {
		try {
			cpiece = cboard.getPiece("e7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial e7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitE8(){ 
		try {
			cpiece = cboard.getPiece("e8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial e8");
		}
		assertEquals(true,(cpiece instanceof King));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitF1() {
		try {
			cpiece = cboard.getPiece("f1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial f1");
		}
		assertEquals(true,(cpiece instanceof Bishop));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitF2() {
		try {
			cpiece = cboard.getPiece("f2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial f2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitF7() {
		try {
			cpiece = cboard.getPiece("f7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial f7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitF8(){ 
		try {
			cpiece = cboard.getPiece("f8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial f8");
		}
		assertEquals(true,(cpiece instanceof Bishop));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitG1() {
		try {
			cpiece = cboard.getPiece("g1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial g1");
		}
		assertEquals(true,(cpiece instanceof Knight));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitG2() {
		try {
			cpiece = cboard.getPiece("g2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial g2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitG7() {
		try {
			cpiece = cboard.getPiece("g7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial g7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitG8(){ 
		try {
			cpiece = cboard.getPiece("g8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial g8");
		}
		assertEquals(true,(cpiece instanceof Knight));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitH1() {
		try {
			cpiece = cboard.getPiece("h1");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial h1");
		}
		assertEquals(true,(cpiece instanceof Rook));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitH2() {
		try {
			cpiece = cboard.getPiece("h2");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial h2");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.WHITE);
	}

	@Test
	public void testInitH7() {
		try {
			cpiece = cboard.getPiece("h7");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial h7");
		}
		assertEquals(true,(cpiece instanceof Pawn));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}

	@Test
	public void testInitH8(){ 
		try {
			cpiece = cboard.getPiece("h8");
		} catch (IllegalPositionException e) {
			fail("Incorrect Initial h8");
		}
		assertEquals(true,(cpiece instanceof Rook));
		assertEquals(cpiece.getColor(),Color.BLACK);
	}


	@Test
	public void testInitNull() {
		for (int i=0; i<8; i++) {
			for (int j=2; j<6; j++ ) {
				String mypos = colname[i].concat(rowname[j]);
				try {
					cpiece = cboard.getPiece(mypos);
				} catch (IllegalPositionException e) {
					String myerr = "Incorrect Initial " + mypos;
					fail(myerr);
				}
				assertEquals(cpiece,null);
			}
		}
	}

	//SET POSITION TESTS
	@Test(expected = IllegalPositionException.class)
	public void testSetPosition9() throws IllegalPositionException {
		bpiece.setPosition("a9");
	}

	@Test(expected = IllegalPositionException.class)
	public void testSetPositionQ() throws IllegalPositionException {
		bpiece.setPosition("q7");
	}

	@Test
	public void testSetPositionB3() {
		try {
			bpiece.setPosition("b3");;
		} catch (IllegalPositionException e) {
			fail("Incorrect Set Position B3");
		}
		assertEquals(bpiece.getPosition(),"b3");
	}

	//PLACE PIECE TESTS
	@Test
	public void testPlacePieceNull() {
		Boolean ptest = cboard.placePiece(cpiece, "a0");
		assertEquals(ptest,false);
	}

	@Test
	public void testPlacePieceNotSet() {
		Boolean ptest = cboard.placePiece(bpiece,  "e5");
		assertEquals(ptest,true);
		assertEquals(bpiece.getPosition(),"e5");

		try {
			cpiece = cboard.getPiece("e5");
		} catch (IllegalPositionException e) {
			fail("Improper Add E5 Not Set");
		}
		assertEquals(cpiece.getColor(),Color.BLACK);
		assertEquals(true,(cpiece instanceof Pawn));
	}

	@Test
	public void testPlacePieceSet() {
		try {
			bpiece.setPosition("f6");
		} catch (IllegalPositionException e) {
			fail("Incorrect Place Position Set 1");
		}

		Boolean ptest = cboard.placePiece(bpiece,"f3");
		assertEquals(ptest,true);
		assertEquals(bpiece.getPosition(),"f3");

		try {
			cpiece = cboard.getPiece("f6");
		} catch (IllegalPositionException e) {
			fail("Incorrect Place Position Set 2");
		}
		assertEquals(cpiece,null);

		try {
			cpiece = cboard.getPiece("f3");
		} catch (IllegalPositionException e) {
			fail("Incorrect Place Position Set 3");
		}
		assertEquals(cpiece.getColor(),Color.BLACK);
		assertEquals(true,(cpiece instanceof Pawn));
	}


	//MOVE TESTS
	@Test(expected = IllegalMoveException.class)
	public void testMoveEmptyPieceFrom() throws IllegalMoveException {
		cboard.move("a4", "a1");
	}

	@Test(expected = IllegalMoveException.class)
	public void testInvalidPieceFrom() throws IllegalMoveException {
		cboard.move("aa4",  "a1");
	}

	@Test(expected = IllegalMoveException.class)
	public void testInvalidPieceTo() throws IllegalMoveException{
		cboard.move("g5",  "q8");
	}

	@Test(expected = IllegalMoveException.class)
	public void testMoveBadBlackPawn() throws IllegalMoveException {
		cboard.move("a7","b7");
	}

	@Test(expected = IllegalMoveException.class)
	public void testMoveBadWhitePawn() throws IllegalMoveException {
		cboard.move("h2","h1");
	}

	@Test
	public void testMoveToSelf() {
		try {
			cboard.move("h2", "h2");
		} catch (IllegalMoveException e) {
			fail("Implemented Allowing Move To Self");
		}
		try {
			cpiece = cboard.getPiece("h2");
		} catch (IllegalPositionException e) {
			fail("Implemented Allowing Move To Self");
		}
		Boolean valid = (cpiece instanceof ChessPiece); 
		assertEquals(valid, true);
	}

	@Test
	public void testQueenMove() {
		cpiece = new Queen(cboard,Color.BLACK);
		Boolean valid = cboard.placePiece(cpiece,  "e6");
		if (!valid) {
			fail("Fail Putting Piece to E6");
		}
		valid = testValidMove("e6","e7",cboard,Color.BLACK);
		assertEquals(valid,false);
	}

	@Test(expected = IllegalMoveException.class)
	public void testBlackQueenMove() throws IllegalMoveException{
		cboard.move("d8","d7");
	}

	@Test(expected = IllegalMoveException.class)
	public void testWhiteQueenMove() throws IllegalMoveException{
		cboard.move("d1","e1");
	}

	@Test(expected = IllegalMoveException.class)
	public void testnewQueenMove() throws IllegalMoveException{
		ChessPiece myqueen = new Queen(cboard,Color.BLACK);
		cboard.placePiece(myqueen,  "f4");
		cboard.move("f4", "f5");
	}

	@Test
	public void testQueenToSelf() {
		try {
			cboard.move("d8", "d8");
		} catch (IllegalMoveException e) {
			fail("Implemented Allowing Move To Self");
		}

		ChessPiece piece = null;
		try {
			piece = cboard.getPiece("d8");
		} catch (IllegalPositionException e) {
			fail("Bad Get Queen");
		}
		Boolean valid = (piece instanceof Queen);
		assertEquals(valid,true);
	}

	@Test(expected = IllegalMoveException.class)
	public void testBlackKnightMove() throws IllegalMoveException{
		cboard.move("b8","d7");
	}

	@Test(expected = IllegalMoveException.class)
	public void testWhiteKnightMove() throws IllegalMoveException{
		cboard.move("b1","e1");
	}

	@Test(expected = IllegalMoveException.class)
	public void testnewKnightMove() throws IllegalMoveException{
		ChessPiece myknight = new Knight(cboard,Color.BLACK);
		Boolean valid = cboard.placePiece(myknight,  "f4");
		if (!valid) {
			fail("Error Placing New Knight");
		}
		cboard.move("f4", "f5");
	}

	@Test
	public void testKnightToSelf() {
		try {
			cboard.move("b8", "b8");
		} catch (IllegalMoveException e) {
			fail("Implemented Allowing Move To Self");
		}

		ChessPiece piece = null;
		try {
			piece = cboard.getPiece("b8");
		} catch (IllegalPositionException e) {
			fail("Bad Get Knight");
		}
		Boolean valid = (piece instanceof Knight);
		assertEquals(valid,true);
	}


	@Test
	public void testPawnMovePawn() {
		try {
			cboard.move("f7", "f6");
		} catch (IllegalMoveException e) {
			fail("Pawn Move Bad");
		}

		ChessPiece piece = null;
		try {
			piece = cboard.getPiece("f6");
		} catch (IllegalPositionException e) {
			fail("Pawn Get Bad");
		}
		Boolean valid = (piece instanceof Pawn);
		assertEquals(valid,true);
	}

	@Test
	public void testKingMoveKing() {
		ChessPiece piece = new King(cboard,Color.BLACK);
		Boolean valid = cboard.placePiece(piece,  "a4");
		if (!valid) {
			fail("Error Putting Piece");
		}

		try {
			cboard.move("a4", "b4");
		} catch (IllegalMoveException e) {
			fail("Piece Move Bad");
		}

		try {
			piece = cboard.getPiece("b4");
		} catch (IllegalPositionException e) {
			fail("Piece Get Bad");
		}
		valid = (piece instanceof King);
		assertEquals(valid,true);
	}

	@Test
	public void testRookMoveRook() {
		ChessPiece piece = new Rook(cboard,Color.BLACK);
		Boolean valid = cboard.placePiece(piece,  "a4");
		if (!valid) {
			fail("Error Putting Piece");
		}

		try {
			cboard.move("a4", "g4");
		} catch (IllegalMoveException e) {
			fail("Piece Move Bad");
		}

		try {
			piece = cboard.getPiece("g4");
		} catch (IllegalPositionException e) {
			fail("Piece Get Bad");
		}
		valid = (piece instanceof Rook);
		assertEquals(valid,true);
	}

	@Test
	public void testBishopMoveBishop() {
		ChessPiece piece = new Bishop(cboard,Color.BLACK);
		Boolean valid = cboard.placePiece(piece,  "a4");
		if (!valid) {
			fail("Error Putting Piece");
		}

		try {
			cboard.move("a4", "b5");
		} catch (IllegalMoveException e) {
			fail("Piece Move Bad");
		}

		try {
			piece = cboard.getPiece("b5");
		} catch (IllegalPositionException e) {
			fail("Piece Get Bad");
		}
		valid = (piece instanceof Bishop);
		assertEquals(valid,true);
	}

	@Test
	public void testWhitePawnMoveOne() {
		Boolean valid = testValidMove("a2", "a3",  cboard,  Color.WHITE);
		assertEquals(valid,true);
	}

	@Test
	public void testWhitePawnMoveTwo() {
		Boolean valid = testValidMove("c2", "c4", cboard, Color.WHITE);
		assertEquals(valid,true);
	}

	@Test
	public void testBlackPawnCapture() {
		Boolean valid = cboard.placePiece(wpiece,  "g6");
		if (!valid) {
			fail("Fail Putting Piece To G6");
		}
		valid = testValidMove("h7", "g6", cboard, Color.BLACK);
		assertEquals(valid,true);
	}

	@Test
	public void testWhiteRookCapture() {
		cpiece = new Rook(cboard, Color.WHITE);
		Boolean valid = cboard.placePiece(cpiece,  "e4");
		if (!valid) {
			fail("Fail Putting Piece to E4");
		}
		valid = testValidMove("e4","e7",cboard,Color.WHITE);
		assertEquals(valid,true);
	}

	@Test
	public void testBlackBishopMoveFail() {
		cpiece = new Bishop(cboard, Color.BLACK);
		Boolean valid = cboard.placePiece(cpiece,  "b7");
		if (!valid) {
			fail("Fail Putting Piece to B7");
		}
		valid = testValidMove("b7","h1",cboard,Color.BLACK);
		assertEquals(valid,false);
	}

	@Test
	public void testBlackBishopMoveCapture() {
		cpiece = new Bishop(cboard, Color.BLACK);
		Boolean valid = cboard.placePiece(cpiece,  "b7");
		if (!valid) {
			fail("Fail Putting Piece to B7");
		}
		valid = testValidMove("b7","g2",cboard,Color.BLACK);
		assertEquals(valid,true);
	}

	@Test
	public void testBlackBishopMove() {
		cpiece = new Bishop(cboard, Color.BLACK);
		Boolean valid = cboard.placePiece(cpiece,  "c3");
		if (!valid) {
			fail("Fail Putting Piece to C3");
		}
		valid = testValidMove("c3","b4",cboard,Color.BLACK);
		assertEquals(valid,true);
	}

	@Test
	public void testBlackKingMoveFail() {
		Boolean valid = testValidMove("e8","e6",cboard,Color.BLACK);
		assertEquals(valid,false);
	}

	////////////////////////////////////
	// Private Routine To Test Moving //
	private Boolean testValidMove(String fromPos, String toPos, 
			ChessBoard bboard, Color bcolor) {

		//Move
		//System.out.println(bboard);
		try {
			bboard.move(fromPos, toPos);
		} catch (IllegalMoveException e) {
			return false;
		}
		//System.out.println(bboard);

		//Test pawn movement
		ChessPiece fromPiece = null;
		ChessPiece toPiece = null;
		try {
			fromPiece = bboard.getPiece(fromPos);
			toPiece = bboard.getPiece(toPos);
		} catch (IllegalPositionException e) {
			return false;
		}

		if (fromPiece != null) {
			return false;
		}

		if (toPiece.getColor() != bcolor) {
			return false;
		}

		String nowPos = toPiece.getPosition();
		if (!nowPos.contentEquals(toPos)) {
			return false;
		}

		return true;

	}
}
