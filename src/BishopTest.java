package a1;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import a1.ChessPiece.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BishopTest {

	private ChessBoard rboard;
	private ChessPiece myPiece;
	private Bishop wb;
	private Bishop bb;
	private String value;
	private ArrayList<String> myList;

	@Before
	public void setUp() {
		rboard = new ChessBoard();
		rboard.initialize();
		wb = new Bishop(rboard,Color.WHITE);
		bb = new Bishop(rboard,Color.BLACK);
		value = "";
		myList = new ArrayList<String>();
	}

	///TEST STRINGS
	@Test
	public void testBlackBishopString() {
		value = bb.toString();
		assertEquals(value,"\u265D");
	}

	@Test
	public void testWhiteBishopString() {
		value = wb.toString();
		assertEquals(value,"\u2657");
	}

	///TEST LEGAL MOVES
	@Test
	public void testBishopWhiteBlockedLegalMoves() {
		try {
			myPiece = rboard.getPiece("f1");
		} catch (IllegalPositionException e) {
			fail ("Illegal Bishop F1 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBishopBlackBlockedLegalMoves() {
		try {
			myPiece = rboard.getPiece("f8");
		} catch (IllegalPositionException e) {
			fail ("Illegal Bishop F8 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		Collections.sort(mymoves);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBishopWhiteCenterLegalMoves() {
		Boolean ptest = rboard.placePiece(wb, "e4");
		if (ptest == false) {
			fail("Illegal Place Bishop E4");
		}
		ArrayList<String> mymoves = wb.legalMoves();
		List<String> myListAdd1 = Arrays.asList("b7","c6","d5","f3");
		List<String> myListAdd2 = Arrays.asList("d3","f5","g6","h7");
		myList.addAll(myListAdd1);
		myList.addAll(myListAdd2);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testBishopBlackCenterLegalMoves() {
		Boolean ptest = rboard.placePiece(bb, "e5");
		if (ptest == false) {
			fail("Illegal Place Bishop E5");
		}
		ArrayList<String> mymoves = bb.legalMoves();
		List<String> myListAdd1 = Arrays.asList("b2","c3","d4","f6");
		List<String> myListAdd2 = Arrays.asList("d6","f4","g3","h2");
		myList.addAll(myListAdd1);
		myList.addAll(myListAdd2);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

}
