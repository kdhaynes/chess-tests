package a1;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import a1.ChessPiece.Color;
import java.util.ArrayList;

public class KnightTest {

	private ChessBoard rboard;
	private ChessPiece mypiece;

	@Before
	public void setUp() {
		rboard = new ChessBoard();
		rboard.initialize();
	}

	////TEST STRINGS
	@Test
	public void testBlackKnightString() {
		Knight bk = new Knight(rboard,Color.BLACK);
		String value = bk.toString();
		assertEquals(value,"\u265E");
	}

	@Test
	public void testWhiteKnightString() {
		Knight wk = new Knight(rboard,Color.WHITE);
		String value = wk.toString();
		assertEquals(value,"\u2658");
	}

	////TEST LEGAL MOVES
	@Test
	public void testWhiteKnightLegalMoves() {
		try {
			mypiece = rboard.getPiece("b8");
		} catch (IllegalPositionException e) {
			fail("Error Getting Black Knight");
		}
		ArrayList<String> mymoves = mypiece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

	@Test
	public void testBlackKnightLegalMoves() {
		try {
			mypiece = rboard.getPiece("b1");
		} catch (IllegalPositionException e) {
			fail("Error Getting White Knight");
		}
		ArrayList<String> mymoves = mypiece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

	@Test
	public void testRandomBlackKnightLegalMoves() {
		mypiece = new Knight(rboard,Color.BLACK);
		Boolean valid = rboard.placePiece(mypiece, "c3");
		if (!valid) {
			fail("Error Putting Random Black Knight");
		}
		ArrayList<String> mymoves = mypiece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

}
