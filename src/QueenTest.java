package a1;

import static org.junit.Assert.*;
import org.junit.Test;
import a1.ChessPiece.Color;
import java.util.ArrayList;

public class QueenTest {

	////TEST STRINGS
	@Test
	public void testBlackQueenString() {
		ChessBoard rboard = new ChessBoard();
		Queen bq = new Queen(rboard,Color.BLACK);
		String value = bq.toString();
		assertEquals(value,"\u265B");
	}

	@Test
	public void testWhiteQueenString() {
		ChessBoard rboard = new ChessBoard();
		Queen wq = new Queen(rboard,Color.WHITE);
		String value = wq.toString();
		assertEquals(value,"\u2655");
	}

	////TEST LEGAL MOVES
	@Test
	public void testWhiteQueenMoves() {
		ChessBoard rboard = new ChessBoard();
		rboard.initialize();

		ChessPiece piece = null;
		try {
			piece = rboard.getPiece("d8");
		} catch (IllegalPositionException e) {
			fail("Error Getting Black Queen");
		}
		ArrayList<String> mymoves = piece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

	@Test
	public void testBlackQueenMoves() {
		ChessBoard rboard = new ChessBoard();
		rboard.initialize();

		ChessPiece piece = null;
		try {
			piece = rboard.getPiece("d1");
		} catch (IllegalPositionException e) {
			fail("Error Getting White Queen");
		}
		ArrayList<String> mymoves = piece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

	@Test
	public void testRandomBlackQueenMoves() {
		ChessBoard rboard = new ChessBoard();
		rboard.initialize();
		Queen mypiece = new Queen(rboard,Color.BLACK);
		Boolean valid = rboard.placePiece(mypiece,"f4");
		if (!valid) {
			fail("Error Putting Random Black Queen");
		}
		ArrayList<String> mymoves = mypiece.legalMoves();
		assertTrue(mymoves.isEmpty());
	}

}