package a1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.Before;
import a1.ChessPiece.Color;

public class RookTest {

	private ChessBoard rboard;
	private ChessPiece myPiece;
	private ArrayList<String> myList;

	@Before 
	public void setUp() {
		rboard = new ChessBoard();
		rboard.initialize();
		myList = new ArrayList<String>();
	}

	//String Checks
	@Test
	public void testBlackRookString() {
		Rook r = new Rook(rboard,Color.BLACK);
		String value = r.toString();
		assertEquals(value,"\u265C");
	}

	@Test
	public void testWhiteRookString() {
		Rook r = new Rook(rboard,Color.WHITE);
		String value = r.toString();
		assertEquals(value,"\u2656");
	}

	//Legal Moves Checks
	@Test
	public void testRookH1LegalMoves() {
		try {
			myPiece = rboard.getPiece("h1");
		} catch (IllegalPositionException e) {
			fail ("Illegal Rook H1 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testRookA1LegalMoves() {
		try {
			myPiece = rboard.getPiece("a1");
		} catch (IllegalPositionException e) {
			fail("Illegal Rook A1 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testRookA8LegalMoves() {
		try {
			myPiece = rboard.getPiece("a8");
		} catch (IllegalPositionException e) {
			fail("Illegal Rook A8 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testRookAHLegalMoves() {
		try {
			myPiece = rboard.getPiece("h8");
		} catch (IllegalPositionException e) {
			fail("Illegal Rook H8 Get");
		}
		ArrayList<String> mymoves = myPiece.legalMoves();
		assertEquals(mymoves,myList);
	}

	@Test
	public void testRookWhiteCenterLegalMoves() {
		Rook myrook = new Rook(rboard,Color.WHITE);
		Boolean ptest = rboard.placePiece(myrook, "d5");
		if (ptest == false) {
			fail("Illegal Place Rook D5");
		}
		ArrayList<String> mymoves = myrook.legalMoves();
		List<String> myListAdd1 = Arrays.asList("d3","d4","d6","d7");
		List<String> myListAdd2 = Arrays.asList("a5","b5","c5","e5","f5","g5","h5");
		myList.addAll(myListAdd1);
		myList.addAll(myListAdd2);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

	@Test
	public void testRookBlackCenterLegalMoves() {
		Rook myrook = new Rook(rboard,Color.BLACK);
		Boolean ptest = rboard.placePiece(myrook, "e4");
		if (ptest == false) {
			fail("Illegal Place Rook D5");
		}
		ArrayList<String> mymoves = myrook.legalMoves();
		List<String> myListAdd1 = Arrays.asList("e2","e3","e5","e6");
		List<String> myListAdd2 = Arrays.asList("a4","b4","c4","d4","f4","g4","h4");
		myList.addAll(myListAdd1);
		myList.addAll(myListAdd2);
		Collections.sort(mymoves);
		Collections.sort(myList);
		assertEquals(mymoves,myList);
	}

}