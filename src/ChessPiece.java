package a1;

import java.util.ArrayList;

public abstract class ChessPiece {


	public enum Color {WHITE, BLACK}

	protected ChessBoard board; // the board it belongs to, default null
	protected int row; // the index of the horizontal rows 0..7
	protected int column; // the index of the vertical column 0..7
	protected Color color; // the color of the piece

	protected String[] rowname = {"1","2","3","4","5","6","7","8"};
	protected String[] colname = {"a","b","c","d","e","f","g","h"};

	public ChessPiece(ChessBoard board, Color color) {
		this.row = -1;
		this.column = -1;
		this.board = board;
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public String getPosition() {
		String mypos = "";
		if ((this.column > -1) && (this.row > -1)) {
			if ((this.column < 8) && (this.row < 8)) {
				mypos=colname[this.column].concat(rowname[this.row]);
			}}
		return mypos;
	}

	public void setPosition(String position) throws IllegalPositionException {
		char mycolumn = position.charAt(0);
		switch(mycolumn) {
		case 'a':
			this.column = 0; break;
		case 'b':
			this.column = 1; break;
		case 'c':
			this.column = 2; break;
		case 'd':
			this.column = 3; break;
		case 'e':
			this.column = 4; break;
		case 'f':
			this.column = 5; break;
		case 'g':
			this.column = 6; break;
		case 'h':
			this.column = 7; break;
		default:
			throw new IllegalPositionException("Bad Column Position");
		}

		char myrow = position.charAt(1);
		switch(myrow) {
		case '1':
			this.row = 0; break;
		case '2':
			this.row = 1; break;
		case '3':
			this.row = 2; break;
		case '4':
			this.row = 3; break;
		case '5':
			this.row = 4; break;
		case '6':
			this.row = 5; break;
		case '7':
			this.row = 6; break;
		case '8':
			this.row = 7; break;
		default:
			throw new IllegalPositionException("Bad Row Position");
		}
	}

	abstract public String toString();
	abstract public ArrayList<String> legalMoves();

}
