package a1;

import a1.ChessPiece.Color;
import java.util.ArrayList;

public class ChessBoard {

	private ChessPiece[][] board;

	public ChessBoard() {
		board = new ChessPiece[8][8];
	}

	public void initialize() {

		// Rooks
		Rook r1 = new Rook(this, Color.BLACK);
		placePiece(r1, "a8");
		Rook r2 = new Rook(this, Color.BLACK);
		placePiece(r2, "h8");
		Rook r3 = new Rook(this, Color.WHITE);
		placePiece(r3, "a1");
		Rook r4 = new Rook(this, Color.WHITE);
		placePiece(r4, "h1");

		// Pawns
		Pawn p1 = new Pawn(this, Color.BLACK);
		placePiece(p1, "a7");
		Pawn p2 = new Pawn(this, Color.BLACK);
		placePiece(p2, "b7");
		Pawn p3 = new Pawn(this, Color.BLACK);
		placePiece(p3, "c7");
		Pawn p4 = new Pawn(this, Color.BLACK);
		placePiece(p4, "d7");
		Pawn p5 = new Pawn(this, Color.BLACK);
		placePiece(p5, "e7");
		Pawn p6 = new Pawn(this, Color.BLACK);
		placePiece(p6, "f7");
		Pawn p7 = new Pawn(this, Color.BLACK);
		placePiece(p7, "g7");
		Pawn p8 = new Pawn(this, Color.BLACK);
		placePiece(p8, "h7");

		Pawn p9 = new Pawn(this, Color.WHITE);
		placePiece(p9, "a2");
		Pawn p10 = new Pawn(this, Color.WHITE);
		placePiece(p10, "b2");
		Pawn p11 = new Pawn(this, Color.WHITE);
		placePiece(p11, "c2");
		Pawn p12 = new Pawn(this, Color.WHITE);
		placePiece(p12, "d2");
		Pawn p13 = new Pawn(this, Color.WHITE);
		placePiece(p13, "e2");
		Pawn p14 = new Pawn(this, Color.WHITE);
		placePiece(p14, "f2");
		Pawn p15 = new Pawn(this, Color.WHITE);
		placePiece(p15, "g2");
		Pawn p16 = new Pawn(this, Color.WHITE);
		placePiece(p16, "h2");

		// Knights
		Knight k1 = new Knight(this, Color.BLACK);
		placePiece(k1, "b8");
		Knight k2 = new Knight(this, Color.BLACK);
		placePiece(k2, "g8");
		Knight k3 = new Knight(this, Color.WHITE);
		placePiece(k3, "b1");
		Knight k4 = new Knight(this, Color.WHITE);
		placePiece(k4, "g1");

		// Bishops
		Bishop b1 = new Bishop(this, Color.BLACK);
		placePiece(b1, "c8");
		Bishop b2 = new Bishop(this, Color.BLACK);
		placePiece(b2, "f8");
		Bishop b3 = new Bishop(this, Color.WHITE);
		placePiece(b3, "c1");
		Bishop b4 = new Bishop(this, Color.WHITE);
		placePiece(b4, "f1");

		// Queens
		Queen q1 = new Queen(this, Color.BLACK);
		placePiece(q1, "d8");
		Queen q2 = new Queen(this, Color.WHITE);
		placePiece(q2, "d1");

		// Kings
		King kg1 = new King(this, Color.BLACK);
		placePiece(kg1, "e8");
		King kg2 = new King(this, Color.WHITE);
		placePiece(kg2, "e1");
	}

	public ChessPiece getPiece(String position) throws IllegalPositionException {
		int mycol = getColumn(position);
		int myrow = getRow(position);
		return board[myrow][mycol];
	}

	public boolean placePiece(ChessPiece piece, String position) {
		if (piece == null) {
			return false;
		}

		String mypos = piece.getPosition();
		if (mypos != "") {
			try {
				int mycol = getColumn(mypos);
				int myrow = getRow(mypos);
				board[myrow][mycol] = null;
			} catch (IllegalPositionException e) {
				return false;
			}
		}
		try {
			piece.setPosition(position);
			int mycol = getColumn(position);
			int myrow = getRow(position);
			board[myrow][mycol] = piece;
			return true;
		} catch (IllegalPositionException e) {
			System.err.println("!!Place Piece Add Error!!");
			return false;
		}
	}

	public void move(String fromPosition, String toPosition) throws IllegalMoveException {
		if (fromPosition.contentEquals(toPosition)) {
			return;
		}
		
		try {
			ChessPiece mypiece = getPiece(fromPosition);
			if (mypiece == null) {
				throw new IllegalMoveException("!!Null Space!!");
			} else {
				ArrayList<String> mymoves = mypiece.legalMoves();
				if (mymoves == null) {
					throw new IllegalMoveException("!!No Possible Move!!");
				}
				if (mymoves.contains(toPosition)) {
					try {  // if piece at toPosition, set to null
						ChessPiece mydeadpiece = getPiece(toPosition);
						if (mydeadpiece != null) {
							mydeadpiece = null;
						}
					} catch(IllegalPositionException e) {
						throw new IllegalMoveException();
					}

					placePiece(mypiece,toPosition);
				} else {
					throw new IllegalMoveException();
				}
			}

		} catch (IllegalPositionException e){
			throw new IllegalMoveException("TESTING");
		}
	}

	public String toString() {
		String chess = "";
		String upperLeft = "\u250C";
		String upperRight = "\u2510";
		String horizontalLine = "\u2500";
		String horizontal3 = horizontalLine + "\u3000" + horizontalLine;
		String verticalLine = "\u2502";
		String upperT = "\u252C";
		String bottomLeft = "\u2514";
		String bottomRight = "\u2518";
		String bottomT = "\u2534";
		String plus = "\u253C";
		String leftT = "\u251C";
		String rightT = "\u2524";

		String topLine = upperLeft;
		for (int i = 0; i < 7; i++) {
			topLine += horizontal3 + upperT;
		}
		topLine += horizontal3 + upperRight;

		String bottomLine = bottomLeft;
		for (int i = 0; i < 7; i++) {
			bottomLine += horizontal3 + bottomT;
		}
		bottomLine += horizontal3 + bottomRight;
		chess += topLine + "\n";

		for (int row = 7; row >= 0; row--) {
			String midLine = "";
			for (int col = 0; col < 8; col++) {
				if (board[row][col] == null) {
					midLine += verticalLine + " \u3000 ";
				} else {
					midLine += verticalLine + " " + board[row][col] + " ";
				}
			}
			midLine += verticalLine;
			String midLine2 = leftT;
			for (int i = 0; i < 7; i++) {
				midLine2 += horizontal3 + plus;
			}
			midLine2 += horizontal3 + rightT;
			chess += midLine + "\n";
			if (row >= 1)
				chess += midLine2 + "\n";
		}

		chess += bottomLine;
		return chess;
	}

	public static void main(String[] args) {
	//	ChessBoard board = new ChessBoard();
	//	board.initialize();
	//	System.out.println(board);
	//  try {
	// 	     board.move("a4", "a6");
	//		 System.out.println(board);
	//	 } catch (IllegalMoveException e) {
	//	      System.err.print("!!!Illegal Move!!!");
	//	     e.printStackTrace();
	//	 }
	}

	private Integer getColumn(String position) throws IllegalPositionException {
		char mystringcol = position.charAt(0);
		int mycol = -1;
		switch (mystringcol) {
		case 'a':
			mycol = 0; break;
		case 'b':
			mycol = 1; break;
		case 'c':
			mycol = 2; break;
		case 'd':
			mycol = 3; break;
		case 'e':
			mycol = 4; break;
		case 'f':
			mycol = 5; break;
		case 'g':
			mycol = 6; break;
		case 'h':
			mycol = 7; break;
		default:
			throw new IllegalPositionException("Invalid Column");
		}
		return mycol;
	}

	private Integer getRow(String position) throws IllegalPositionException {
		char mystringrow = position.charAt(1);
		int myrow = -1;
		switch (mystringrow) {
		case '1':
			myrow = 0; break;
		case '2':
			myrow = 1; break;
		case '3':
			myrow = 2; break;
		case '4':
			myrow = 3; break;
		case '5':
			myrow = 4; break;
		case '6':
			myrow = 5; break;
		case '7':
			myrow = 6; break;
		case '8':
			myrow = 7; break;
		default:
			throw new IllegalPositionException();
		}
		return myrow;
	}

}
