package a1;

import java.util.ArrayList;

public class Bishop extends ChessPiece {

	public Bishop(ChessBoard board, Color color) {
		super(board, color);
	}

	@Override
	public String toString() {
		if (color == Color.BLACK) {
			return "\u265D";
		} else {
			return "\u2657";
		}
	}

	@Override
	public ArrayList<String> legalMoves() {
		ArrayList<String> mymoves = new ArrayList<String>();

		// Check moves up-left
		mymoves = legalAdds(-1, 1, mymoves);
		// Check moves up-right
		mymoves = legalAdds(1, 1, mymoves);
		// Check moves down-left
		mymoves = legalAdds(-1, -1, mymoves);
		// Check moves down-right
		mymoves = legalAdds(1, -1, mymoves);

		return mymoves;
	}

	private String getNewPosition(int newcol, int newrow) {
		String newpos = colname[newcol].concat(rowname[newrow]);
		return newpos;
	}

	private ArrayList<String> legalAdds(int cdir, int rdir, 
			ArrayList<String> mymoves) {
		Boolean valid = true;
		ChessPiece trypiece;
		String trypos;
		int trycol = column;
		int tryrow = row;
		do {
			trycol += cdir;
			tryrow += rdir;
			if ((trycol > -1) && (trycol < 8) && (tryrow > -1) && (tryrow < 8)) {
				trypos = getNewPosition(trycol,tryrow);
				try {
					trypiece = board.getPiece(trypos);
					if (trypiece == null) {
						mymoves.add(trypos);
					} else if (trypiece.getColor() != color) {
						mymoves.add(trypos);
						valid = false;
					} else {
						valid = false;
					}
				} catch (IllegalPositionException e) {
					valid = false;
				}
			} else {
				valid = false;
			}
		}while(valid);

		return mymoves;
	}

}
